package com.space4gis.position.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;
import com.space4gis.position.PreferenceManager;
import com.space4gis.position.R;

import java.util.ArrayList;
import java.util.List;

public class AboutActivity extends AhoyOnboarderActivity {
    PreferenceManager pref;
    String first;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = new PreferenceManager(this);
        first = pref.getConnect();

        AhoyOnboarderCard ahoyOnboarderCard1 = new AhoyOnboarderCard("Position", "Votre position en temps réel.", R.drawable.ic_place_white_24dp);
        AhoyOnboarderCard ahoyOnboarderCard2 = new AhoyOnboarderCard("Creez vos propres adresses", "Creez et personnalisez dès à présent vos adresses grâce à notre application.", R.drawable.ic_add_location_white_24dp);
        AhoyOnboarderCard ahoyOnboarderCard3 = new AhoyOnboarderCard("Navigation", "Un puissant système de navigation qui vous accompagne dans tous vos déplacements.", R.drawable.ic_navigation_white_24dp);

        ahoyOnboarderCard1.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard2.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard3.setBackgroundColor(R.color.black_transparent);

        List<AhoyOnboarderCard> pages = new ArrayList<>();

        pages.add(ahoyOnboarderCard1);
        pages.add(ahoyOnboarderCard2);
        pages.add(ahoyOnboarderCard3);

        for (AhoyOnboarderCard page : pages) {
            page.setTitleColor(R.color.white);
            page.setDescriptionColor(R.color.grey_200);
        }

        setFinishButtonTitle("Allez-y");
        showNavigationControls(true);
        //setGradientBackground();
        setImageBackground(R.drawable.img3);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        //setFont(face);

        setInactiveIndicatorColor(R.color.grey_600);
        setActiveIndicatorColor(R.color.white);

        setOnboardPages(pages);

    }

    @Override
    public void onFinishButtonPressed() {
        Intent intent = new Intent(AboutActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onStart () {
        super.onStart();
        if (first.matches("no")) {

        }
         else {
            Intent intent = new Intent(AboutActivity.this, AccueilActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
