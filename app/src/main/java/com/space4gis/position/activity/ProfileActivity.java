package com.space4gis.position.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.space4gis.position.Function;
import com.space4gis.position.R;
import com.space4gis.position.UserBDD;
import com.space4gis.position.api.APIClient;
import com.space4gis.position.api.ApiInterface;
import com.space4gis.position.model.ImageModel;
import com.space4gis.position.model.Profile;
import com.space4gis.position.model.ResponseModel;
import com.space4gis.position.model.User;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "Profile";
    UserBDD userBDD;

    ApiInterface apiInterface;
    private Profile profile;
    String email;
    private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;
    User user;

    @BindView(R.id.user_name)
    TextView txt_name;
    @BindView(R.id.user_email)
    TextView txt_email;
    @BindView(R.id.user_phone)
    TextView txt_phone;
    @BindView(R.id.user_pp)
    CircularImageView txt_pp;
    @BindView(R.id.user_lieu)
    TextView txt_lieu;
    @BindView(R.id.user_carte)
    TextView txt_carte;
    @BindView(R.id.user_adresse)
    TextView txt_adresse;
    @BindView(R.id.user_probleme)
    TextView txt_probleme;
    @BindView(R.id.user_setting)
    TextView txt_setting;
    @BindView(R.id.button)
    Button txt_button;

    private Bitmap bitmap;
    private Uri filePath;
    String image;

    private int PICK_IMAGE_REQUEST = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setIndeterminate(false);
        // Progress dialog horizontal style
        progressDialog.setProgressStyle(R.style.AppLoginTheme);
        // Progress dialog title
        progressDialog.setMessage("Mise à jour de la Photo...");

        mAuth = FirebaseAuth.getInstance();

        ButterKnife.bind(this);

        userBDD = new UserBDD(this);
        userBDD.open();

        email = mAuth.getCurrentUser().getEmail();



        user = userBDD.getUserWithEmail(email);


     /*   Context context = getApplicationContext();
        CharSequence text = user.getImage();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();*/

        txt_button.setVisibility(View.GONE);

        if(user != null) {
            txt_name.setText(user.getName());
            txt_email.setText(user.getEmail());
            txt_phone.setText(user.getTelephone());
            txt_lieu.setText(user.getLieux());
            txt_carte.setText(user.getCarte());
            txt_adresse.setText(user.getAdresse());

           if (user.getImage().equals("NULL")) {
               txt_pp.setImageResource(R.drawable.prof);
            }

            else{
               Glide.with(this)
                       .load(user.getImage())
                       .into(txt_pp);

             //  Picasso.get().load(user.getImage()).into(txt_pp);
           }


        }

        txt_pp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        txt_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                uploadImage();
            }
        });


    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Selectionner une image"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                image = getStringImage(bitmap);
                txt_pp.setImageBitmap(bitmap);
                txt_button.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 30, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage =  Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void uploadImage() {

            if (Function.isNetworkAvailable(getApplicationContext())) {

                ApiInterface apiService =
                        APIClient.getClient8().create(ApiInterface.class);
                Call<ImageModel> call = apiService.upload(email,"a,"+image);
                Log.d("email11",email);
                Log.d("image11","a,"+image);
                call.enqueue(new Callback<ImageModel>() {
                    @Override
                    public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                        int code = response.code();
                        String c = String.valueOf(code);
                        Log.d("code11",c);
                        if(response.body() == null) {
                            progressDialog.dismiss();
                            Context context = getApplicationContext();
                            CharSequence text = "Erreur d'enregistrement";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                        }

                        else {

                            Context context = getApplicationContext();
                            CharSequence text = "Upload reussi";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                            user = userBDD.getUserWithEmail(email);
                            user.setImage(response.body().getUrl());
                            userBDD.updateUser(Integer.parseInt(user.getId()),user);

                            progressDialog.dismiss();


                            Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
                            startActivity(intent);
                            finish();
                        }





                    }

                    @Override
                    public void onFailure(Call<ImageModel> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("main2", t.toString());
                        progressDialog.dismiss();
                    }
                });
            }
            else{
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }


    }



    @Override
    public void onStart() {
        super.onStart();

    }
}
