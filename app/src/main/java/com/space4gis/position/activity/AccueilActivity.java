package com.space4gis.position.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.inputmethodservice.Keyboard;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Adapter;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.android.gestures.AndroidGesturesManager;
import com.mapbox.android.gestures.StandardGestureDetector;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.feedback.FeedbackBottomSheetListener;
import com.mapbox.services.android.navigation.ui.v5.feedback.FeedbackItem;
import com.mapbox.services.android.navigation.ui.v5.listeners.FeedbackListener;
import com.mapbox.services.android.navigation.ui.v5.listeners.NavigationListener;
import com.mapbox.services.android.navigation.v5.navigation.MapboxNavigation;
import com.space4gis.position.Function;
import com.space4gis.position.PermissionGps;
import com.space4gis.position.PreferenceManager;
import com.space4gis.position.R;
import com.space4gis.position.UserBDD;
import com.space4gis.position.adapter.SearchAdapter;
import com.space4gis.position.adapter.SearchPositionAdapter;
import com.space4gis.position.api.APIClient;
import com.space4gis.position.api.ApiInterface;
import com.space4gis.position.listenner.RecyclerTouchListener;
import com.space4gis.position.model.Nominatim;
import com.space4gis.position.model.Position;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.space4gis.position.model.PositionCoord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;


import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.space4gis.position.model.ResponseModel;
import com.space4gis.position.model.User;

public class AccueilActivity extends AppCompatActivity implements
        OnMapReadyCallback, PermissionsListener,NavigationView.OnNavigationItemSelectedListener
,MapboxMap.OnMapClickListener, NavigationListener, FeedbackListener , LocationListener {
    private FirebaseAuth mAuth;
    private PermissionsManager permissionsManager;
    private MapboxMap mapboxMap;
    private MapView mapView;
    UserBDD userBDD;
    private UiSettings settings;
    @BindView(R.id.floatingActionButton)
    FloatingActionButton fab;
    @BindView(R.id.floatingActionButton1)
    FloatingActionButton fab1;
    @BindView(R.id.floatingActionButton2)
    FloatingActionButton fab2;
    @BindView(R.id.floatingActionButton3)
    FloatingActionButton fab3;
    @BindView(R.id.floatingActionButton4)
    FloatingActionButton fab4;
   // private MaterialSearchBar searchBar;
    ApiInterface apiInterface;
    private List<Position> pos;
    private PositionCoord posc;
    private PositionCoord posc1;
    private List<Double> origine;
    private List<String> l;
    private String code,nom,position,comune;
    private  List<Nominatim> nomin;
    PreferenceManager pref;
    private  Marker marker;
    private LocationComponent locationComponent;
    private DirectionsRoute currentRoute;
    private static final String TAG = "Activity";
    private NavigationMapRoute navigationMapRoute;
    private List<Nominatim> suggestions = new ArrayList<>();
    private List<Position> suggestionsP = new ArrayList<>();
    private SearchAdapter customSuggestionsAdapter;
    private SearchPositionAdapter customSuggestionsAdapterP;
    private SearchView search;
    private RecyclerView recyclerView;
    private String mQueryString;
    private NavigationListener navigationListener;
    private ProgressDialog dialog;
    User user;
    String email100 = "";

    final Handler handler = new Handler();

    public LocationManager locationManager;
    public Criteria criteria;
    String bestProvider;
    LocationManager loc = null;
    private String fournisseur;





    @SuppressLint({"MissingPermission", "RestrictedApi"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));
        setContentView(R.layout.activity_accueil);
        mAuth = FirebaseAuth.getInstance();
        ButterKnife.bind(this);
        dialog = new ProgressDialog(this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
       // searchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
        search = (SearchView) findViewById(R.id.search);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        search.setQueryHint("Rechercher");
        search.setActivated(true);
        search.onActionViewExpanded();
        search.setIconified(false);
        search.clearFocus();
        pos = new ArrayList<>();
        nomin = new ArrayList<>();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        fab1.setVisibility(View.GONE);
        fab2.setVisibility(View.GONE);
        fab3.setVisibility(View.VISIBLE);
        fab4.setVisibility(View.VISIBLE);

        userBDD = new UserBDD(this);
        userBDD.open();

        try {
            email100 = mAuth.getCurrentUser().getEmail();

            user = userBDD.getUserWithEmail(email100);
        } catch (Exception e) {
            Intent intent = new Intent(AccueilActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }


        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchNominatim(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
               if(newText.length() == 0 || newText.matches("")) {
                   recyclerView.setVisibility(View.GONE);
               }
               else {
                   textChanged(newText);
               }
                return true;
            }
        });


        pref = new PreferenceManager(this);
        posc = new PositionCoord();
        posc1 = new PositionCoord();
        l = new ArrayList<>();
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LocationComponent locationComponent = mapboxMap.getLocationComponent();

                locationComponent.setCameraMode(CameraMode.TRACKING);
                locationComponent.setRenderMode(RenderMode.COMPASS);
                locationComponent.zoomWhileTracking(18);


            }
        });

        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
                drawer.openDrawer(GravityCompat.START);
            }
        });

        fab4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog();
            }
        });

        initialiserLocalisation();

    /*    double lat = locationComponent.getLastKnownLocation().getLatitude();
        double lng = locationComponent.getLastKnownLocation().getLongitude();
        String ln = String.valueOf(lng);
        String la = String.valueOf(lat);

        updateP(ln,la);*/

    }

    @SuppressLint({"MissingPermission", "RestrictedApi"})
    private void initialiserLocalisation()
    {
        if(locationManager == null)
        {
            locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            Criteria criteres = new Criteria();

            criteres.setAccuracy(Criteria.ACCURACY_FINE);

            criteres.setAltitudeRequired(true);

            criteres.setBearingRequired(true);

            criteres.setSpeedRequired(true);

            criteres.setCostAllowed(true);
            criteres.setPowerRequirement(Criteria.POWER_HIGH);

            fournisseur = locationManager.getBestProvider(criteres, true);
            Log.d("GPS", "fournisseur : " + fournisseur);
        }

        try {
            Location localisation = loc.getLastKnownLocation(fournisseur);

                String la = String.valueOf(localisation.getLatitude());
                String ln = String.valueOf(localisation.getLongitude());

                updateP(ln,la);


        } catch (Exception e) {
            locationManager.requestLocationUpdates(fournisseur, 1000, 0,this);
        }





    }

    public void textChanged(String text) {
        recyclerView.setVisibility(View.VISIBLE);
        mQueryString = text;
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Function.isNetworkAvailable(getApplicationContext())) {
                    ApiInterface apiService1 = APIClient.getClient4().create(ApiInterface.class);
                    Call<List<Nominatim>> call = apiService1.nominatim(mQueryString,"json",1,"cm");
                    call.enqueue(new Callback<List<Nominatim>>() {
                        @SuppressLint("RestrictedApi")
                        @Override
                        public void onResponse(Call<List<Nominatim>> call, Response<List<Nominatim>> response) {
                            if (response.body().isEmpty()) {
                                mQueryString = mQueryString.replace(" ",",");
                                ApiInterface apiService =
                                        APIClient.getClient5().create(ApiInterface.class);
                                Call<List<Position>> call1 = apiService.position(mQueryString);
                                call1.enqueue(new Callback<List<Position>>() {
                                    @SuppressLint("RestrictedApi")
                                    @Override
                                    public void onResponse(Call<List<Position>> call, Response<List<Position>> response) {

                                        pos = response.body();
                                        customSuggestionsAdapterP = new SearchPositionAdapter(pos,R.layout.item_custom_suggestion,getApplicationContext(),mapboxMap,AccueilActivity.this,recyclerView,search,navigationMapRoute,fab1,fab2,mapView,locationComponent,currentRoute);
                                recyclerView.setAdapter(customSuggestionsAdapterP);
                                customSuggestionsAdapterP.notifyDataSetChanged();

                            }

                            @Override
                            public void onFailure(Call<List<Position>> call, Throwable t) {
                                // Log error here since request failed
                                Log.e("main2", t.toString());
                            }
                        });


                    }

                            else {
                        nomin = response.body();
                        customSuggestionsAdapter = new SearchAdapter(nomin,R.layout.item_custom_suggestion,getApplicationContext(),mapboxMap,AccueilActivity.this,recyclerView,search,navigationMapRoute,fab1,fab2,mapView,locationComponent,currentRoute,dialog);
                                recyclerView.setAdapter(customSuggestionsAdapter);
                                customSuggestionsAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onFailure(Call<List<Nominatim>> call, Throwable t) {
                            Log.e("main3", t.toString());
                        }
                    });

                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        }, 300);


    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void searchPosition (String search) {
        search = search.replace(" ",",");
        ApiInterface apiService =
                APIClient.getClient().create(ApiInterface.class);
        Call<List<Position>> call = apiService.position(search);
        call.enqueue(new Callback<List<Position>>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(Call<List<Position>> call, Response<List<Position>> response) {
                if (response.body().isEmpty()) {
                    Context context = getApplicationContext();
                    CharSequence text = "Aucune zone trouvée";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
                mapboxMap.clear();
                if (navigationMapRoute != null) {
                    navigationMapRoute.removeRoute();
                    fab1.setVisibility(View.GONE);
                }
                pos = response.body();
                if (pos.size() > 1) {
                    Context context = getApplicationContext();
                    CharSequence text1 = "Soyez plus précis";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text1, duration);
                    toast.show();
                } else {
                        code = response.body().get(0).getCode();
                        nom = response.body().get(0).getNom();
                        position = response.body().get(0).getPosition();
                        origine = response.body().get(0).getOrigine();
                        double lng = origine.get(0);
                        double lat = origine.get(1);

                        CameraPosition cam = new CameraPosition.Builder()
                                .target(new LatLng(lat, lng))
                                .zoom(17)
                                .build();

                        mapboxMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lng))
                                .title("nom :" + nom + " " + "position :" + position));

                    fab2.setVisibility(View.VISIBLE);
                    fab2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            navigation(new LatLng(lat,lng));
                        }
                    });

                        mapboxMap.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener(){
                            @Override
                            public boolean onMarkerClick(@NonNull Marker marker) {
                                return false;
                            }
                        });


                        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cam),5000);
                        AccueilActivity.this.search.setQuery("",false);
                        AccueilActivity.this.search.clearFocus();

                }


            }

            @Override
            public void onFailure(Call<List<Position>> call, Throwable t) {
                // Log error here since request failed
                Log.e("main2", t.toString());
            }
        });
    }

    public PositionCoord searchCoord(String search) {
        if (Function.isNetworkAvailable(getApplicationContext())) {
            ApiInterface apiService =
                    APIClient.getClient2().create(ApiInterface.class);
            Call<PositionCoord> call = apiService.positionCoord(search);
            call.enqueue(new Callback<PositionCoord>() {
                @Override
                public void onResponse(Call<PositionCoord> call, Response<PositionCoord> response) {
                    if (response.body() == null) {
                        Context context = getApplicationContext();
                        CharSequence text = "Aucune zone trouvée";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                    posc = response.body();
                }

                @Override
                public void onFailure(Call<PositionCoord> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("main2", t.toString());
                }
            });
        }
        else{
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }

        return posc;


    }

    public void searchNominatim(String query) {
        if (query.matches("")) {
            Context context = getApplicationContext();
            CharSequence text1 = "Aucune entrée";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text1, duration);
            toast.show();
        } else {
            if (Function.isNetworkAvailable(getApplicationContext())) {
                ApiInterface apiService1 = APIClient.getNewClient().create(ApiInterface.class);
                Call<List<Nominatim>> call = apiService1.nominatim(query,"json",1,"cm");
                call.enqueue(new Callback<List<Nominatim>>() {
                    @SuppressLint("RestrictedApi")
                    @Override
                    public void onResponse(Call<List<Nominatim>> call, Response<List<Nominatim>> response) {
                        if (response.body().isEmpty()) {
                            searchPosition(query);
                        }
                        else {
                            mapboxMap.clear();
                            if (navigationMapRoute != null) {
                                navigationMapRoute.removeRoute();
                                fab1.setVisibility(View.GONE);
                            }
                            nomin = response.body();
                            if (nomin.size()> 1) {
                                Context context = getApplicationContext();
                                CharSequence text1 = "Soyez plus précis";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text1, duration);
                                toast.show();
                            }
                            else {
                                double lat = Double.parseDouble(nomin.get(0).getLat());
                                double lng = Double.parseDouble(nomin.get(0).getLon());
                                String display_name = nomin.get(0).getDisplayName();

                                String p = lng+","+lat;
                                posc1 = searchCoord(p);

                                CameraPosition cam = new CameraPosition.Builder()
                                        .target(new LatLng(lat, lng))
                                        .zoom(17)
                                        .build();

                                mapboxMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(lat, lng))
                                        .title(display_name));

                                fab2.setVisibility(View.VISIBLE);
                                fab2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        navigation(new LatLng(lat,lng));
                                    }
                                });

                                mapboxMap.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener(){
                                    @Override
                                    public boolean onMarkerClick(@NonNull Marker marker) {
                                        Async3 task = new Async3(AccueilActivity.this);
                                        task.execute(new String[]{nomin.get(0).getLon(),nomin.get(0).getLat()});
                                        return false;
                                    }
                                });
                                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cam),5000);
                                search.setQuery("",false);
                                search.clearFocus();
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<List<Nominatim>> call, Throwable t) {
                        Log.e("main3", t.toString());
                    }
                });
            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
        }
    }

    @SuppressLint({"MissingPermission", "RestrictedApi"})
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_pos) {
            dialog();
        }/* else if (id == R.id.nav_share) {
            double lat = locationComponent.getLastKnownLocation().getLatitude();
            double lng = locationComponent.getLastKnownLocation().getLongitude();
            String ln = String.valueOf(lng);
            String la = String.valueOf(lat);

            if (Function.isNetworkAvailable(getApplicationContext())) {
                Async2 task = new Async2(AccueilActivity.this);
                task.execute(new String[]{ln,la});
            }

        }*/ else if (id == R.id.nav_profil) {
            Intent intent = new Intent(AccueilActivity.this, ProfileActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_pb) {
            Context context = getApplicationContext();
            CharSequence text1 = "Pas encore disponible";
            int duration = Toast.LENGTH_LONG;

            Toast toast = Toast.makeText(context, text1, duration);
            toast.show();
        } else if (id == R.id.nav_clear) {
            mapboxMap.clear();
            fab2.setVisibility(View.GONE);
            if (navigationMapRoute != null) {
                navigationMapRoute.removeRoute();
                fab1.setVisibility(View.GONE);
            }
        }/* else if (id == R.id.nav_download) {
            Context context = getApplicationContext();
            CharSequence text1 = "Pas encore disponible";
            int duration = Toast.LENGTH_LONG;

            Toast toast = Toast.makeText(context, text1, duration);
            toast.show();
        }*/ else if (id == R.id.nav_logout) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(AccueilActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @SuppressLint({"MissingPermission", "RestrictedApi"})
    public void updateP(String ln,String la) {
        Log.d("test10000","okay");
        if (Function.isNetworkAvailable(getApplicationContext())) {
            ApiInterface apiService =
                    APIClient.getClient9().create(ApiInterface.class);
            Call<ResponseModel> call = apiService.updatePosition(email100,ln,la);
            call.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    if (response.body() != null) {
                        Log.d("position100","yes");
                    }

                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("main2", t.toString());
                }
            });
        }
    }

    public void alertDialog() {
        Dialog dialog;
        final String[] items = {"STREET", "SATELLITE", "SATELLITE-STREET", "DARK", "LIGHT","OUTDOORS","TRAFFIC-DAY","TRAFFIC-NIGHT"};
        int checkItem = 0;
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this,R.style.AppRegisterTheme));
        builder.setTitle("Selectionnez votre style : ");
        builder.setSingleChoiceItems(items, checkItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        newStyle(Style.MAPBOX_STREETS);
                        dialog.dismiss();
                        break;
                    case 1:
                        newStyle(Style.SATELLITE);
                        dialog.dismiss();
                        break;
                    case 2:
                        newStyle(Style.SATELLITE_STREETS);
                        dialog.dismiss();
                        break;
                    case 3:
                        newStyle(Style.DARK);
                        dialog.dismiss();
                        break;
                    case 4:
                        newStyle(Style.LIGHT);
                        dialog.dismiss();
                        break;
                    case 5:
                        newStyle(Style.OUTDOORS);
                        dialog.dismiss();
                        break;
                    case 6:
                        newStyle(Style.TRAFFIC_DAY);
                        dialog.dismiss();
                        break;
                    case 7:
                        newStyle(Style.TRAFFIC_NIGHT);
                        dialog.dismiss();
                        break;
                }

            }
        });
        AlertDialog ad = builder.create();
        ad.show();
    }

    @SuppressLint({"MissingPermission", "RestrictedApi"})
    private void dialog () {
        mapboxMap.clear();
        if (navigationMapRoute != null) {
            navigationMapRoute.removeRoute();
            fab1.setVisibility(View.GONE);
        }
        LocationComponent locationComponent = mapboxMap.getLocationComponent();
        locationComponent.setCameraMode(CameraMode.TRACKING);
        locationComponent.zoomWhileTracking(18);
        locationComponent.setRenderMode(RenderMode.COMPASS);
       double lat = locationComponent.getLastKnownLocation().getLatitude();
       double lng = locationComponent.getLastKnownLocation().getLongitude();
      String ln = String.valueOf(lng);
      String la = String.valueOf(lat);
        if (Function.isNetworkAvailable(getApplicationContext())) {
            Async task = new Async(AccueilActivity.this);
            task.execute(new String[]{ln,la});
        }



    }

    public void newStyle (String style) {
        mapboxMap.setStyle(style , new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                addDestinationIconSymbolLayer(style);
                mapboxMap.addOnMapClickListener(AccueilActivity.this);
                fab1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean simulateRoute = false;
                        if (currentRoute != null) {
                        NavigationLauncherOptions options = NavigationLauncherOptions.builder()
                                .directionsRoute(currentRoute)
                                .shouldSimulateRoute(simulateRoute)
                                .build();

                            NavigationLauncher.startNavigation(AccueilActivity.this, options);
                        }
                        else {
                            Context context = getApplicationContext();
                            CharSequence text1 = "Patientez un instant et reesayez";
                            int duration = Toast.LENGTH_LONG;

                            Toast toast = Toast.makeText(context, text1, duration);
                            toast.show();



                        }
                    }
                });
            }
        });
        UiSettings uiSettings = mapboxMap.getUiSettings();
        uiSettings.setLogoEnabled(false);
        uiSettings.setAttributionEnabled(false);


    }


    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        AccueilActivity.this.mapboxMap = mapboxMap;

        mapboxMap.setStyle(Style.MAPBOX_STREETS , new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);
                        addDestinationIconSymbolLayer(style);
                        mapboxMap.addOnMapClickListener(AccueilActivity.this);
                        fab1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                boolean simulateRoute = false;
                                if (currentRoute != null) {
                                NavigationLauncherOptions options = NavigationLauncherOptions.builder()
                                        .directionsRoute(currentRoute)
                                        .shouldSimulateRoute(simulateRoute)
                                        .build();

                                    NavigationLauncher.startNavigation(AccueilActivity.this, options);
                                }
                                else {
                                    Context context = getApplicationContext();
                                    CharSequence text1 = "Patientez un instant et reesayez";
                                    int duration = Toast.LENGTH_SHORT;

                                    Toast toast = Toast.makeText(context, text1, duration);
                                    toast.show();


                                }

                            }
                        });
                    }
                });
        UiSettings uiSettings = mapboxMap.getUiSettings();
        uiSettings.setLogoEnabled(false);
        uiSettings.setAttributionEnabled(false);
    }

    private void addDestinationIconSymbolLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));
        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        loadedMapStyle.addSource(geoJsonSource);
        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id", "destination-source-id");
        destinationSymbolLayer.withProperties(
                iconImage("destination-icon-id"),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        );
        loadedMapStyle.addLayer(destinationSymbolLayer);
    }

    @SuppressLint({"RestrictedApi","MissingPermission"})
    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
        mapboxMap.clear();
        if (navigationMapRoute != null) {
            navigationMapRoute.removeRoute();
            fab1.setVisibility(View.GONE);
        }
        recyclerView.setVisibility(View.GONE);
        mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(point.getLatitude(), point.getLongitude())));
        String ln = String.valueOf(point.getLongitude());
        String la = String.valueOf(point.getLatitude());
       fab2.setVisibility(View.VISIBLE);
       fab2.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               navigation(point);
           }
       });

        if (Function.isNetworkAvailable(getApplicationContext())) {
            Async1 task = new Async1(AccueilActivity.this,drawer);
            task.execute(new String[]{ln,la});
        }


        return true;
    }

    @SuppressLint("MissingPermission")
    public void navigation(LatLng point) {
        Point destinationPoint = Point.fromLngLat(point.getLongitude(), point.getLatitude());
         Point originPoint = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                locationComponent.getLastKnownLocation().getLatitude());

    /*    GeoJsonSource source = mapboxMap.getStyle().getSourceAs("destination-source-id");
        if (source != null) {
            source.setGeoJson(Feature.fromGeometry(destinationPoint));
        }*/

        getRoute(originPoint, destinationPoint);
    }

    @SuppressLint("RestrictedApi")
    private void getRoute(Point origin, Point destination) {
        if (navigationMapRoute != null) {
            navigationMapRoute.removeRoute();
        }
        dialog.setIndeterminate(false);
        dialog.setProgressStyle(R.style.AppATheme);
        dialog.setMessage("Recherche d'un itinéraire...");
        dialog.show();

        if (Function.isNetworkAvailable(getApplicationContext())) {
            NavigationRoute.builder(AccueilActivity.this)
                    .accessToken(Mapbox.getAccessToken())
                    .origin(origin)
                    .destination(destination)
                    .build()
                    .getRoute(new Callback<DirectionsResponse>() {
                        @Override
                        public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
// You can get the generic HTTP info about the response
                            Log.d(TAG, "Response code: " + response.code());
                            if (response.body() == null) {
                                Log.e(TAG, "No routes found, make sure you set the right user and access token.");
                                return;
                            } else if (response.body().routes().size() < 1) {
                                Log.e(TAG, "No routes found");
                                return;
                            }

                            currentRoute = response.body().routes().get(0);

                                dialog.dismiss();
                                if (navigationMapRoute != null) {
                                    navigationMapRoute.removeRoute();
                                } else {
                                    navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.NavigationMapRoute);
                                }
                                navigationMapRoute.addRoute(currentRoute);


// Draw the route on the map

                        }

                        @Override
                        public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                            Log.e(TAG, "Error: " + throwable.getMessage());
                        }
                    });
            fab1.setVisibility(View.VISIBLE);
            fab1.setEnabled(true);
            fab1.setBackgroundResource(R.color.colorPrimary);
        }

        else {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }

    }

    @SuppressWarnings( {"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this, loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission Accordée", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(this, "Permission non accordée", Toast.LENGTH_LONG).show();

                }
                return;
            }
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, "Demande de permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);
                }
            });
        } else {
            Toast.makeText(this, "Permission non accordée", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    @SuppressWarnings( {"MissingPermission"})
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser == null) {
            Intent intent = new Intent(AccueilActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

     /*   LocationManager loc;
        loc = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!loc.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }*/

        else {
            mapView.onStart();
         //   updateP();
            initialiserLocalisation();

        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
        if (mapboxMap != null) {
            mapboxMap.clear();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if (mapboxMap != null) {
            mapboxMap.clear();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onCancelNavigation() {
        finish();
    }

    @Override
    public void onNavigationFinished() {
        finish();
    }

    @Override
    public void onNavigationRunning() {

    }

    public void zoom () {

    }

    @Override
    public void onFeedbackOpened() {

    }

    @Override
    public void onFeedbackCancelled() {

    }

    @Override
    public void onFeedbackSent(FeedbackItem feedbackItem) {
    }


    @Override
    public void onLocationChanged(Location location) {
        locationManager.removeUpdates(this);

        String la = String.valueOf(location.getLatitude());
        String ln = String.valueOf(location.getLongitude());

        updateP(ln,la);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}

class Async1 extends AsyncTask<String, Void, PositionCoord> {

    public Async1(AppCompatActivity activity,DrawerLayout drawerLayout) {
        this.activity = activity;
        this.drawerLayout = drawerLayout;
    }

    private AppCompatActivity activity;
    private PositionCoord posc;
    private DrawerLayout drawerLayout;



    @Override
    protected PositionCoord doInBackground(String... lists) {
        PositionCoord pos = null;
        String p = lists[0]+","+lists[1];
            if (Function.isNetworkAvailable(getApplicationContext())) {
                try {

                ApiInterface apiService =
                        APIClient.getClient3().create(ApiInterface.class);
                Call<PositionCoord> call = apiService.positionCoord(p);
                     pos = call.execute().body();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.d("main6",pos.getPosition());

            }
            else{
                Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }

        return pos;
    }

    @Override
    protected void onPostExecute (PositionCoord result) {

        if (result != null) {
            String posi = result.getPosition();
            String comune = result.getNom();

            Snackbar snackbar = Snackbar
                    .make(drawerLayout,"POSITION :"+posi+"\n"+"COMMUNE :"+comune,Snackbar.LENGTH_INDEFINITE)
                    .setAction("reduire", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }

        else {
            Snackbar snackbar = Snackbar
                    .make(drawerLayout,"Connection error",Snackbar.LENGTH_INDEFINITE);
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }

    }


}

class Async3 extends AsyncTask<String, Void, PositionCoord> {

    public Async3(AppCompatActivity activity) {
        this.activity = activity;
        dialog = new ProgressDialog(activity);
    }

    private ProgressDialog dialog;
    private AppCompatActivity activity;
    private PositionCoord posc;


    @Override
    protected void onPreExecute () {
        this.dialog.setIndeterminate(false);
        this.dialog.setProgressStyle(R.style.AppATheme);
        this.dialog.setMessage("Recherche...");
        this.dialog.show();

    }

    @Override
    protected PositionCoord doInBackground(String... lists) {
        PositionCoord pos = null;
        String p = lists[0]+","+lists[1];
        if (Function.isNetworkAvailable(getApplicationContext())) {
            try {

                ApiInterface apiService =
                        APIClient.getClient3().create(ApiInterface.class);
                Call<PositionCoord> call = apiService.positionCoord(p);
                pos = call.execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("main6",pos.getPosition());

        }
        else{
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }

        return pos;
    }

    @Override
    protected void onPostExecute (PositionCoord result) {
        if (result != null){
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            Log.d("main7", "onPostExecute: "+result);
            String posi = result.getPosition();
            String comune = result.getNom();
            LatLng p = new LatLng(result.getOrigine().get(1),result.getOrigine().get(0));

            AlertDialog.Builder local = new AlertDialog.Builder(new ContextThemeWrapper(activity,R.style.AppRegisterTheme));
            local.setTitle("Données POSITION")
                    .setMessage("POSITION :"+posi+"\n"+"COMMUNE :"+comune)
                     .setPositiveButton("Partager", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                             String lien = "https://drive.google.com/file/d/19j6pTexYnQbqX6gxMdxW993lsQYUsQtH/view?usp=sharing";
                             Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                             sharingIntent.setType("text/plain");
                             String shareBody = "l'adresse :" + "**"+ posi + " "+comune+"**"+" vient d'être partagée avec vous "+"\n"+"l'application Position est téléchargeable depuis le lien :"+lien;
                             sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Position");
                             sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                             activity.startActivity(Intent.createChooser(sharingIntent, "Partager par"));
                         }
                     })
                    .setCancelable(true);


            local.create().show();
        }

        else {
            AlertDialog.Builder local = new AlertDialog.Builder(new ContextThemeWrapper(activity,R.style.AppRegisterTheme));
            local.setTitle("Error")
                    .setMessage("Connection error")
                    .setCancelable(true);


            local.create().show();
        }

    }
}

 class Async extends AsyncTask<String, Void, PositionCoord> {

    public Async(AppCompatActivity activity) {
        this.activity = activity;
        dialog = new ProgressDialog(activity);
    }

    private ProgressDialog dialog;
    private AppCompatActivity activity;
    private PositionCoord posc;


    @Override
    protected void onPreExecute () {
        this.dialog.setIndeterminate(false);
        this.dialog.setProgressStyle(R.style.AppATheme);
        this.dialog.setMessage("Recherche...");
        this.dialog.show();

    }

    @Override
    protected PositionCoord doInBackground(String... lists) {
        PositionCoord pos = null;
        String p = lists[0]+","+lists[1];
        if (Function.isNetworkAvailable(getApplicationContext())) {
            try {

                ApiInterface apiService =
                        APIClient.getClient3().create(ApiInterface.class);
                Call<PositionCoord> call = apiService.positionCoord(p);
                pos = call.execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("main6",pos.getPosition());

        }
        else{
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }

        return pos;
    }

    @Override
    protected void onPostExecute (PositionCoord result) {
        if (result != null){
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            Log.d("main7", "onPostExecute: "+result);
            String posi = result.getPosition();
            String comune = result.getNom();
            LatLng p = new LatLng(result.getOrigine().get(1),result.getOrigine().get(0));

            AlertDialog.Builder local = new AlertDialog.Builder(new ContextThemeWrapper(activity,R.style.AppRegisterTheme));
            local.setTitle("Données POSITION")
                    .setMessage("POSITION :"+posi+"\n"+"COMMUNE :"+comune)
                    .setPositiveButton("Partager", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String lien = "https://drive.google.com/file/d/19j6pTexYnQbqX6gxMdxW993lsQYUsQtH/view?usp=sharing";
                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent.setType("text/plain");
                            String shareBody = "Retrouve moi sur Position avec l'adresse:" + "**"+ posi + " "+comune+"**"+"\n"+"l'application position est téléchargeable depuis le lien :"+lien;
                            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Ma Position");
                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                            activity.startActivity(Intent.createChooser(sharingIntent, "Partager par"));
                        }
                    })
                    .setCancelable(true);


            local.create().show();
        }

        else {
            AlertDialog.Builder local = new AlertDialog.Builder(new ContextThemeWrapper(activity,R.style.AppRegisterTheme));
            local.setTitle("Error")
                    .setMessage("Connection error")
                    .setCancelable(true);


            local.create().show();
        }

    }
}


class Async2 extends AsyncTask<String, Void, PositionCoord> {

    public Async2(AppCompatActivity activity) {
        this.activity = activity;
        dialog = new ProgressDialog(activity);
    }

    private ProgressDialog dialog;
    private AppCompatActivity activity;
    private PositionCoord posc;


    @Override
    protected void onPreExecute () {
        this.dialog.setIndeterminate(false);
        this.dialog.setProgressStyle(R.style.AppATheme);
        this.dialog.setMessage("Recherche...");
        this.dialog.show();

    }

    @Override
    protected PositionCoord doInBackground(String... lists) {
        PositionCoord pos = null;
        String p = lists[0]+","+lists[1];
        if (Function.isNetworkAvailable(getApplicationContext())) {
            try {

                ApiInterface apiService =
                        APIClient.getClient3().create(ApiInterface.class);
                Call<PositionCoord> call = apiService.positionCoord(p);
                pos = call.execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("main6",pos.getPosition());

        }
        else{
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }

        return pos;
    }

    @Override
    protected void onPostExecute (PositionCoord result) {
        if (result != null){
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            Log.d("main7", "onPostExecute: "+result);
            String posi = result.getPosition();
            String comune = result.getNom();
            LatLng p = new LatLng(result.getOrigine().get(1),result.getOrigine().get(0));

            AlertDialog.Builder local = new AlertDialog.Builder(new ContextThemeWrapper(activity,R.style.AppRegisterTheme));
            local.setTitle("Données POSITION")
                    .setMessage("POSITION :"+posi+"\n"+"COMMUNE :"+comune)
                    .setPositiveButton("Partager", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String lien = "https://drive.google.com/file/d/19j6pTexYnQbqX6gxMdxW993lsQYUsQtH/view?usp=sharing";
                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent.setType("text/plain");
                            String shareBody = "Retrouve moi sur Position avec l'adresse:" + "**"+ posi + " "+comune+"**"+"\n"+"l'application position est téléchargeable depuis le lien :"+lien;
                            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Ma Position");
                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                            activity.startActivity(Intent.createChooser(sharingIntent, "Partager par"));
                        }
                    })
                    .setCancelable(true);


            local.create().show();
        }

        else {
            AlertDialog.Builder local = new AlertDialog.Builder(new ContextThemeWrapper(activity,R.style.AppRegisterTheme));
            local.setTitle("Error")
                    .setMessage("Connection error")
                    .setCancelable(true);


            local.create().show();
        }

    }
}
