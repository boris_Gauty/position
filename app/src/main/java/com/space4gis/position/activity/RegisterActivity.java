package com.space4gis.position.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.space4gis.position.R;

public class RegisterActivity extends AppCompatActivity {
    ProgressDialog progressDialog;

    String username,password,cpassword,phone;

    String TAG = "LoginActivity";

    @BindView(R.id.email1)
    EditText txt_uname;
    @BindView(R.id.password1) EditText txt_pwd;
    @BindView(R.id.cpassword) EditText txt_cpwd;
    @BindView(R.id.phone) EditText txt_phone;
    @BindView(R.id.register)
    Button register;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setIndeterminate(false);
        // Progress dialog horizontal style
        progressDialog.setProgressStyle(R.style.AppRegisterTheme);
        // Progress dialog title
        progressDialog.setMessage("Création du compte...");

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();

                username = txt_uname.getText().toString();
                password = txt_pwd.getText().toString();
                cpassword = txt_cpwd.getText().toString();
                phone = txt_phone.getText().toString();

                if (username.matches("") || password.matches("") || cpassword.matches("") || phone.matches("")) {
                    progressDialog.dismiss();

                    Context context = getApplicationContext();
                    CharSequence text = "Remplissez tous les champs";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }

                else {
                    if(cpassword.equals(password)) {
                        mAuth.createUserWithEmailAndPassword(username, password)
                                .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // Sign in success, update UI with the signed-in user's information
                                            Log.d(TAG, "success");
                                            final FirebaseUser user = mAuth.getCurrentUser();
                                            sendConfirmation(user);
                                            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            // If sign in fails, display a message to the user.
                                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                            Toast.makeText(RegisterActivity.this, "failed.",
                                                    Toast.LENGTH_SHORT).show();
                                        }


                                       progressDialog.dismiss();

                                    }
                                });
                    }
                    else {
                        progressDialog.dismiss();

                        Context context = getApplicationContext();
                        CharSequence text = "Entrez des mots de passes identiques";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                }
            }
        });
    }

    public void login(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void sendConfirmation(final FirebaseUser user) {
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {
                            Toast.makeText(RegisterActivity.this,
                                    "Mail de verification envoyé à " + user.getEmail(),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification", task.getException());
                            Toast.makeText(RegisterActivity.this,
                                    "Echec de l'envoi du mail de vérification.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        // [END_EXCLUDE]
                    }
                });
    }
}
