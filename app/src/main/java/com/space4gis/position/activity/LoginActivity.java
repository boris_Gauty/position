package com.space4gis.position.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.space4gis.position.PreferenceManager;
import com.space4gis.position.R;

import java.util.concurrent.TimeUnit;


public class LoginActivity extends AppCompatActivity   {
    ProgressDialog progressDialog;

    String numero,code;

    String TAG = "LoginActivity";

    @BindView(R.id.number)
    EditText txt_numero;
    @BindView(R.id.code_country)
    EditText txt_code;
    @BindView(R.id.continuer)
    Button continuer;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private boolean mVerificationInProgress = false;
    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";
    PreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();

        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(false);
        // Progress dialog horizontal style
        progressDialog.setProgressStyle(R.style.AppLoginTheme);
        // Progress dialog title
        progressDialog.setMessage("Connexion...");
        mAuth.setLanguageCode("fr");
        pref = new PreferenceManager(this);

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                progressDialog.dismiss();
                Log.d(TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]
                confirm();

            }
            @Override
            public void onVerificationFailed(FirebaseException e) {
                progressDialog.dismiss();
                Log.w(TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    Context context = getApplicationContext();
                    CharSequence text1 = "Erreur de numéro de téléphone";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text1, duration);
                    toast.show();
                    Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    Snackbar.make(findViewById(android.R.id.content), "Quota Depassé.",
                            Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }

            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);
                pref.saveCredential(verificationId);
                confirm();


            }
        };



        continuer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();

                numero = txt_numero.getText().toString();
                code = txt_code.getText().toString();

                if (numero.matches("")) {
                    progressDialog.dismiss();

                    Context context = getApplicationContext();
                    CharSequence text = "Remplissez tous les champs";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }
                else {
                    startPhoneNumberVerification(code+" "+numero);

                }
            }
        });
    }

    public void confirm() {
        Intent intent = new Intent(LoginActivity.this, ConfirmActivity.class);
        intent.putExtra("phone",code+" "+numero);
        startActivity(intent);
        finish();
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

        mVerificationInProgress = true;

    }

    @Override
    public void onStart() {
        super.onStart();
        if (mVerificationInProgress) {
            progressDialog.show();
            startPhoneNumberVerification(code+" "+numero);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
    }







    }
