package com.space4gis.position.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.space4gis.position.AppSignatureHashHelper;
import com.space4gis.position.PreferenceManager;
import com.space4gis.position.R;
import com.space4gis.position.SMSReceiver;
import com.space4gis.position.UserBDD;
import com.space4gis.position.api.APIClient;
import com.space4gis.position.api.ApiInterface;
import com.space4gis.position.model.Profile;
import com.space4gis.position.model.User;

import android.app.ProgressDialog;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

public class ConfirmActivity extends AppCompatActivity implements
        SMSReceiver.OTPReceiveListener {

    String number;

    String TAG = "ConfirmActivity";
    private SMSReceiver smsReceiver;

    @BindView(R.id.code)
    EditText txt_code;
    @BindView(R.id.valider)
    Button valider;
    @BindView(R.id.no_code)
    TextView no_code;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private boolean mVerificationInProgress = false;
    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";
    ProgressDialog progressDialog;
    PreferenceManager pref;
    String verification;
    PhoneAuthProvider.ForceResendingToken t = null;
    UserBDD userBDD;
    User user;
    String email;
    String first;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);

        AppSignatureHashHelper appSignatureHashHelper = new AppSignatureHashHelper(this);

        userBDD = new UserBDD(this);
        userBDD.open();

        // This code requires one time to get Hash keys do comment and share key
        Log.d(TAG, "Apps Hash Key: " + appSignatureHashHelper.getAppSignatures().get(0));

        startSMSListener();

        mAuth = FirebaseAuth.getInstance();

      /*  if(mAuth.getCurrentUser() != null) {
            email = mAuth.getCurrentUser().getEmail();
        }*/



        ButterKnife.bind(this);
        pref = new PreferenceManager(this);
        verification = pref.getCredential();
        first = pref.getConnect();

        progressDialog = new ProgressDialog(ConfirmActivity.this);
        progressDialog.setIndeterminate(false);
        // Progress dialog horizontal style
        progressDialog.setProgressStyle(R.style.AppLoginTheme);
        // Progress dialog title
        progressDialog.setMessage("Connexion...");
        mAuth.setLanguageCode("fr");


        Intent intent = getIntent();

        if (intent != null) {
            number = getIntent().getStringExtra("phone");
        }
        else {
            onBackPressed();
        }

        no_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendVerificationCode(number,t);
            }
        });

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                if (!validatePhoneNumber()) {
                    return;
                }
                verifyPhoneNumberWithCode(verification,txt_code.getText().toString());
            }
        });

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]
                signInWithPhoneAuthCredential(credential);

            }
            @Override
            public void onVerificationFailed(FirebaseException e) {
                progressDialog.dismiss();
                Log.w(TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    Context context = getApplicationContext();
                    CharSequence text1 = "Erreur de numéro de téléphone";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text1, duration);
                    toast.show();
                    Intent intent = new Intent(ConfirmActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    Snackbar.make(findViewById(android.R.id.content), "Quota Depassé.",
                            Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }

            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);
                t = token;

            }
        };
    }

    private boolean validatePhoneNumber() {
        String phoneNumber = number;
        if (TextUtils.isEmpty(phoneNumber)) {
            progressDialog.dismiss();
            return false;
        }

        return true;
    }




    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    // [START resend_verification]
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
    // [END resend_verification]

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            if (user.getEmail() == null) {
                                Intent intent = new Intent(ConfirmActivity.this, InfoActivity.class);
                                intent.putExtra("phone",number);
                                startActivity(intent);
                                finish();
                            }

                            else {
                                email = user.getEmail();
                                getU(email);
                                if (first.matches("no")) {
                                    pref.firstConnect("yes");
                                }
                                Intent intent = new Intent(ConfirmActivity.this, AccueilActivity.class);
                                startActivity(intent);
                                finish();
                            }



                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
                                txt_code.setError("Invalid code.");
                                // [END_EXCLUDE]
                            }
                           progressDialog.dismiss();
                        }
                    }
                });
    }

    private void startSMSListener() {
        try {
            smsReceiver = new SMSReceiver();
            smsReceiver.setOTPListener(this);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
           // this.registerReceiver(smsReceiver, intentFilter);
            registerReceiver(smsReceiver, intentFilter);

            SmsRetrieverClient client = SmsRetriever.getClient(this);

            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // API successfully started
                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Fail to start API
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getU(String email) {
        Log.d("main10", "1");
        ApiInterface apiService1 =
                APIClient.getClient7().create(ApiInterface.class);
        Call<Profile> call1 = apiService1.getUser(email);
        call1.enqueue(new Callback<Profile>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(Call<Profile> call1, Response<Profile> response) {
                Log.d("main15", email);

                user = response.body().getUser();
                Log.d("main20", response.body().getUser().getImage());
                userBDD.insertUser(user);
                
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                // Log error here since request failed
                Log.e("main2", t.toString());
            }
        });

    }


    @Override
    public void onOTPReceived(String otp) {
        showToast("OTP Received: " + otp);
        Log.d("otp",otp);
        verifyPhoneNumberWithCode(verification,otp);
        if (smsReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(smsReceiver);
        }
    }

    @Override
    public void onOTPTimeOut() {
        showToast("OTP Time out");
    }

    @Override
    public void onOTPReceivedError(String error) {
        showToast(error);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (smsReceiver != null) {
          //  LocalBroadcastManager.getInstance(this).unregisterReceiver(smsReceiver);
            unregisterReceiver(smsReceiver);
        }
    }


    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


}


