package com.space4gis.position.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.space4gis.position.BaseSql;
import com.space4gis.position.Function;
import com.space4gis.position.PreferenceManager;
import com.space4gis.position.R;
import com.space4gis.position.UserBDD;
import com.space4gis.position.api.APIClient;
import com.space4gis.position.api.ApiInterface;
import com.space4gis.position.model.Profile;

import com.space4gis.position.model.ResponseModel;
import com.space4gis.position.model.User;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoActivity extends AppCompatActivity {

    String email,name,number;
    PreferenceManager pref;
    String first;

    @BindView(R.id.name1)
    EditText txt_name;
    @BindView(R.id.email1)
    EditText txt_email;
    @BindView(R.id.valider1)
    Button valider;
    private FirebaseAuth mAuth;
    ProgressDialog progressDialog;

    UserBDD userBDD;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        pref = new PreferenceManager(this);
        first = pref.getConnect();

        mAuth = FirebaseAuth.getInstance();

        userBDD = new UserBDD(this);
        userBDD.open();


        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(InfoActivity.this);
        progressDialog.setIndeterminate(false);
        // Progress dialog horizontal style
        progressDialog.setProgressStyle(R.style.AppLoginTheme);
        // Progress dialog title
        progressDialog.setMessage("Enregistrement...");

        Intent intent = getIntent();

        if (intent != null) {
            number = getIntent().getStringExtra("phone");
        }
        else {
            onBackPressed();
        }

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                saveUser();
            }
        });
    }

    public void getU(String email) {
        Log.d("main10", "1");
        ApiInterface apiService1 =
                APIClient.getClient7().create(ApiInterface.class);
        Call<Profile> call1 = apiService1.getUser(email);
        call1.enqueue(new Callback<Profile>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(Call<Profile> call1, Response<Profile> response) {

                Log.d("main12", "3");
                user = response.body().getUser();
                userBDD.insertUser(user);


            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                // Log error here since request failed
                Log.e("main2", t.toString());
            }
        });

    }

    public void saveUser() {
        if (Function.isNetworkAvailable(getApplicationContext())) {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        email = txt_email.getText().toString();
        name = txt_name.getText().toString();

        Log.d("name",name);

        if (!email.matches("")) {



            ApiInterface apiService =
                    APIClient.getClient6().create(ApiInterface.class);
            Call<ResponseModel> call = apiService.saveUser(email,name,number);
            call.enqueue(new Callback<ResponseModel>() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {


                        if(response.body().getStatus() != null) {
                            getU(email);
                            pref.firstConnect("yes");
                            Intent intent = new Intent(InfoActivity.this, AccueilActivity.class);
                            currentUser.updateEmail(email);
                            progressDialog.dismiss();
                            startActivity(intent);
                            finish();
                        }


                    else {
                        Context context = getApplicationContext();
                        CharSequence text = "Erreur d'enregistrement";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }




                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("main2", t.toString());
                    progressDialog.dismiss();
                }
            });

        }
        else {
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Remplisez une adresse Mail", Toast.LENGTH_LONG).show();
        }
        }
        else{
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }





    }


}
