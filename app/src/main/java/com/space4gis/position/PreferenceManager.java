package com.space4gis.position;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class PreferenceManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "welcome";
    private static final String EXTRA_LOGIN = "username";
    private static final String EXTRA_PASSWORD = "password";
    private static final String ID = "1";
    private static final String FIRST = "no";
    private static final String SIZE = "2";
    private static final String ELT = "null";
    private static final List<String> l = Arrays.asList("mekaga","marega","melaga");
    private static final String LNG = "longitude";
    private static final String LAT = "lattitude";
    private static final String CODE = "code";
    private static final String TELEPHONE = "000000000";
    private static final String NAME = "name";
    private static final String EMAIL = "mail";
    private static final String LIEUX = "lieux";
    private static final String CARTE = "cartes";
    private static final String ADRESSE = "adresse";

    public PreferenceManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void createLogin(String username, String password,int id) {
        editor.putString(EXTRA_LOGIN,username );
        editor.putString(EXTRA_PASSWORD, password);
        editor.putInt(ID, id);
        editor.apply();
    }

    public void firstConnect(String a) {
        editor.putString(FIRST,a);
        editor.apply();
    }

    public void saveUser(String name,String email,String telephone,int lieu,int carte,int adresse) {
        editor.putString(NAME,name);
        editor.putString(EMAIL,email);
        editor.putString(TELEPHONE,telephone);
        editor.putInt(LIEUX,lieu);
        editor.putInt(CARTE,carte);
        editor.putInt(ADRESSE,adresse);
        editor.apply();
    }

    public String getNom() {
        return pref.getString(NAME,"nom complet");
    }

    public String getEmail() {
        return pref.getString(EMAIL,"email");
    }

    public String getTelephone() {
        return pref.getString(TELEPHONE,"000000000");
    }

    public int getLieux() {
        return pref.getInt(LIEUX,0);
    }

    public int getCarte() {
        return pref.getInt(CARTE,0);
    }

    public int getAdresse() {
        return pref.getInt(ADRESSE,0);
    }


    public void saveCredential(String verificationId) {
        editor.putString(CODE,verificationId);
        editor.apply();
    }

    public String getCredential() {
        return pref.getString(CODE,"code");
    }

    public void createCoord(String lng,String lat) {
        editor.putString(LNG,lng);
        editor.putString(LAT,lat);
        editor.apply();
    }

    public String getLng() {
        return pref.getString(LNG,"longitude");
    }

    public String getLat() {
        return pref.getString(LAT,"lattitude");
    }
   /* public void createList(List<String> list){
        for(int i = 0; i <= list.size()-1; i++)
        {
            editor.putInt(SIZE,list.size());
            editor.putString(ELT+i,list.get(i));
        }

    }

    public List<String> getList(){
        for(int i = 0; i <= pref.getInt(SIZE,2)-1; i++)
        {
            editor.putInt(SIZE,2);
            editor.putString(ELT+i,"null");
            l.add(pref.getString(ELT+i,null));
        }
        return l;
    }*/

    public String getConnect() {
        return pref.getString(FIRST,"no");
    }

    public String getLogin(){
        return pref.getString(EXTRA_LOGIN,"username");

    }
    public String getPassword(){
        return pref.getString(EXTRA_PASSWORD,"password");

    }

    public int getInt(){
        return pref.getInt(ID,0);

    }

}

