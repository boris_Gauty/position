package com.space4gis.position;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class BaseSql extends SQLiteOpenHelper {

    private static final String TABLE_USER = "users";
    private static final String COL_ID = "id";
    private static final String COL_NAME = "name";
    private static final String COL_EMAIL = "email";
    private static final String COL_TELEPHONE = "telephone";
    private static final String COL_LIEUX = "lieux";
    private static final String COL_CARTE = "carte";
    private static final String COL_ADRESSE = "adresse";
    private static final String COL_IMAGE = "image";

    private static final String CREATE_BDD = "CREATE TABLE " + TABLE_USER + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_NAME + " TEXT NOT NULL, " +
            COL_EMAIL + " TEXT NOT NULL, " + COL_TELEPHONE + " TEXT NOT NULL, " + COL_LIEUX + " INTEGER, "
            + COL_CARTE + " INTEGER, " + COL_ADRESSE + " INTEGER, " + COL_IMAGE + " TEXT);";

    public BaseSql(Context context, String name , CursorFactory factory, int version){
        super(context,name,factory,version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + TABLE_USER + ";");
        onCreate(db);
    }
}
