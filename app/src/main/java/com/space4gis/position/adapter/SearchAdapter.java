package com.space4gis.position.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
import com.space4gis.position.Function;
import com.space4gis.position.R;
import com.space4gis.position.activity.AccueilActivity;
import com.space4gis.position.api.APIClient;
import com.space4gis.position.api.ApiInterface;
import com.space4gis.position.model.Nominatim;
import com.space4gis.position.model.PositionCoord;

import java.io.IOException;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {

    private List<Nominatim> nomin;
    private int rowLayout;
    private Context context;
    private MapboxMap mapboxMap;
    private AccueilActivity activity;
    private RecyclerView recyclerView;
    private SearchView search;
    private NavigationMapRoute navigationMapRoute;
    private FloatingActionButton fab1,fab2;
    private MapView mapView;
    private DirectionsRoute currentRoute;
    private LocationComponent locationComponent;
    private ProgressDialog dialog;
    //ImageView image;


    public static class SearchViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView image;



        public SearchViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            image = (ImageView) v.findViewById(R.id.imageView2);
        }
    }

    public SearchAdapter(List<Nominatim> nomin, int rowLayout, Context context , MapboxMap mapboxMap,AccueilActivity activity,RecyclerView recyclerView,SearchView search,NavigationMapRoute navigationMapRoute,FloatingActionButton fab1,FloatingActionButton fab2,MapView mapView,LocationComponent locationComponent,DirectionsRoute currentRoute,ProgressDialog dialog) {
        this.nomin = nomin;
        this.rowLayout = rowLayout;
        this.context = context;
        this.mapboxMap = mapboxMap;
        this.activity = activity;
        this.search = search;
        this.recyclerView = recyclerView;
        this.navigationMapRoute = navigationMapRoute;
        this.fab1 = fab1;
        this.fab2 = fab2;
        this.mapView = mapView;
        this.locationComponent = locationComponent;
        this.currentRoute = currentRoute;
        this.dialog = dialog;
    }

    @Override
    public SearchViewHolder onCreateViewHolder(ViewGroup parent,
                                                int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new SearchViewHolder(view);
    }


    @Override
    public void onBindViewHolder(SearchViewHolder holder, final int position) {

        String t = nomin.get(position).getDisplayName();


        holder.title.setText(nomin.get(position).getDisplayName());

        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickItemNomin(position);
            }
        });
    }

    public void clickItemNomin(int i) {
        mapboxMap.clear();
        if (navigationMapRoute != null) {
            navigationMapRoute.removeRoute();
            fab1.setVisibility(View.GONE);
        }
        double lat = Double.parseDouble(nomin.get(i).getLat());
        double lng = Double.parseDouble(nomin.get(i).getLon());
        String display_name = nomin.get(i).getDisplayName();


        CameraPosition cam = new CameraPosition.Builder()
                .target(new LatLng(lat, lng))
                .zoom(17)
                .build();

        mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .title(display_name));

        fab2.setVisibility(View.VISIBLE);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.navigation(new LatLng(lat,lng));
            }
        });

        mapboxMap.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener(){
            @Override
            public boolean onMarkerClick(@NonNull Marker marker) {
                Async task = new Async(activity);
                task.execute(new String[]{nomin.get(i).getLon(),nomin.get(i).getLat()});
                return false;
            }
        });

        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cam), 5000);
        recyclerView.setVisibility(View.GONE);
        search.clearFocus();
    }

    @Override
    public int getItemCount() {
        return nomin.size();
    }



}

class Async extends AsyncTask<String, Void, PositionCoord> {

    public Async(AppCompatActivity activity) {
        this.activity = activity;
        dialog = new ProgressDialog(activity);
    }

    private ProgressDialog dialog;
    private AppCompatActivity activity;
    private PositionCoord posc;


    @Override
    protected void onPreExecute () {
        this.dialog.setIndeterminate(false);
        this.dialog.setProgressStyle(R.style.AppATheme);
        this.dialog.setMessage("Recherche...");
        this.dialog.show();

    }

    @Override
    protected PositionCoord doInBackground(String... lists) {
        PositionCoord pos = null;
        String p = lists[0]+","+lists[1];
        if (Function.isNetworkAvailable(getApplicationContext())) {
            try {

                ApiInterface apiService =
                        APIClient.getClient3().create(ApiInterface.class);
                Call<PositionCoord> call = apiService.positionCoord(p);
                pos = call.execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("main6",pos.getPosition());

        }
        else{
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }

        return pos;
    }

    @Override
    protected void onPostExecute (PositionCoord result) {
        if (result != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            Log.d("main7", "onPostExecute: "+result);
            String posi = result.getPosition();
            String comune = result.getNom();
            LatLng p = new LatLng(result.getOrigine().get(1),result.getOrigine().get(0));

            AlertDialog.Builder local = new AlertDialog.Builder(new ContextThemeWrapper(activity,R.style.AppRegisterTheme));
            local.setTitle("Données POSITION")
                    .setMessage("POSITION :"+posi+"\n"+"COMMUNE :"+comune)
                    .setCancelable(true);


            local.create().show();
        }

        else {
            AlertDialog.Builder local = new AlertDialog.Builder(new ContextThemeWrapper(activity,R.style.AppRegisterTheme));
            local.setTitle("Error")
                    .setMessage("Connection Error")
                    .setCancelable(true);


            local.create().show();
        }


    }
}




