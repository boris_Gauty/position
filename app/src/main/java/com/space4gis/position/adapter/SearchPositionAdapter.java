package com.space4gis.position.adapter;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
import com.space4gis.position.Function;
import com.space4gis.position.R;
import com.space4gis.position.activity.AccueilActivity;
import com.space4gis.position.model.Nominatim;
import com.space4gis.position.model.Position;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;

public class SearchPositionAdapter extends RecyclerView.Adapter<SearchPositionAdapter.SearchViewHolder> {

    private List<Position> pos;
    private String code,nom,position,comune;
    private List<Double> origine;
    private int rowLayout;
    private Context context;
    private MapboxMap mapboxMap;
    private AccueilActivity activity;
    private RecyclerView recyclerView;
    private SearchView search;
    private NavigationMapRoute navigationMapRoute;
    private FloatingActionButton fab1,fab2;
    private MapView mapView;
    private DirectionsRoute currentRoute;
    private LocationComponent locationComponent;
    //ImageView image;


    public static class SearchViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView image;



        public SearchViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            image = (ImageView) v.findViewById(R.id.imageView2);
        }
    }

    public SearchPositionAdapter(List<Position> pos, int rowLayout, Context context, MapboxMap mapboxMap, AccueilActivity activity, RecyclerView recyclerView, SearchView search, NavigationMapRoute navigationMapRoute, FloatingActionButton fab1, FloatingActionButton fab2, MapView mapView, LocationComponent locationComponent, DirectionsRoute currentRoute) {
        this.pos = pos;
        this.rowLayout = rowLayout;
        this.context = context;
        this.mapboxMap = mapboxMap;
        this.activity = activity;
        this.search = search;
        this.recyclerView = recyclerView;
        this.navigationMapRoute = navigationMapRoute;
        this.fab1 = fab1;
        this.fab2 = fab2;
        this.mapView = mapView;
        this.locationComponent = locationComponent;
        this.currentRoute = currentRoute;
    }

    @Override
    public SearchViewHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new SearchViewHolder(view);
    }


    @Override
    public void onBindViewHolder(SearchViewHolder holder, final int position) {


        holder.title.setText(pos.get(position).getPosition()+","+pos.get(holder.getAdapterPosition()).getNom());

        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickItemPosition(position);
            }
        });

    }

    public void clickItemPosition (int i) {
        mapboxMap.clear();
        if (navigationMapRoute != null) {
            navigationMapRoute.removeRoute();
            fab1.setVisibility(View.GONE);
        }
        code = pos.get(i).getCode();
        nom = pos.get(i).getNom();
        position = pos.get(i).getPosition();
        origine = pos.get(i).getOrigine();
        double lng = origine.get(0);
        double lat = origine.get(1);

        CameraPosition cam = new CameraPosition.Builder()
                .target(new LatLng(lat, lng))
                .zoom(17)
                .build();

        mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .title("nom :" + nom + " " + "position :" + position));

        fab2.setVisibility(View.VISIBLE);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.navigation(new LatLng(lat,lng));
            }
        });

        mapboxMap.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener(){
            @Override
            public boolean onMarkerClick(@NonNull Marker marker) {
                return false;
            }
        });

        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cam), 5000);
        recyclerView.setVisibility(View.GONE);
        search.clearFocus();
    }


    @Override
    public int getItemCount() {
        return pos.size();
    }


}




