package com.space4gis.position.api;

import com.space4gis.position.model.ImageModel;
import com.space4gis.position.model.Nominatim;
import com.space4gis.position.model.Position;
import com.space4gis.position.model.PositionCoord;
import com.space4gis.position.model.Profile;
import com.space4gis.position.model.ResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {


    @GET("prend_mot/{search}")
    Call<List<Position>> position(@Path("search") String search);

    @GET("prend_coord/{search}")
     Call<PositionCoord> positionCoord(@Path("search") String search);

    @POST("saveuser")
    @FormUrlEncoded
    Call<ResponseModel> saveUser(@Field("email") String email, @Field("name") String name, @Field("telephone") String telephone);

    @POST("getuser")
    @FormUrlEncoded
    Call<Profile> getUser(@Field("email") String email);

    @POST("updateposition")
    @FormUrlEncoded
    Call<ResponseModel> updatePosition(@Field("email") String email, @Field("lng") String lng, @Field("lat") String lat);

    @POST("uploadimage")
    @FormUrlEncoded
    Call<ImageModel> upload(@Field("email") String email, @Field("image") String image);

    @GET("search")
    Call<List<Nominatim>> nominatim(@Query("q") String q,@Query("format") String format,@Query("addressdetails") int addressdetails,@Query("countrycodes") String countrycodes);
}
