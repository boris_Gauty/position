package com.space4gis.position;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.space4gis.position.BaseSql;
import com.space4gis.position.model.User;

public class UserBDD {

    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "position.db";

    private static final String TABLE_USER = "users";
    private static final String COL_ID = "id";
    private static final int NUM_COL_ID = 0;
    private static final String COL_NAME = "name";
    private static final int NUM_COL_NAME = 1;
    private static final String COL_EMAIL = "email";
    private static final int NUM_COL_EMAIL = 2;
    private static final String COL_TELEPHONE = "telephone";
    private static final int NUM_COL_TELEPHONE = 3;
    private static final String COL_LIEUX = "lieux";
    private static final int NUM_COL_LIEUX = 4;
    private static final String COL_CARTE = "carte";
    private static final int NUM_COL_CARTE = 5;
    private static final String COL_ADRESSE = "adresse";
    private static final int NUM_COL_ADRESSE = 6;
    private static final String COL_IMAGE = "image";
    private static final int NUM_COL_IMAGE = 7;

    private SQLiteDatabase bdd;

    private BaseSql baseSql;

    public UserBDD(Context context){
        //On crée la BDD et sa table
        baseSql = new BaseSql(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = baseSql.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    public long insertUser(User user){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_ID, user.getId());
        values.put(COL_NAME, user.getName());
        values.put(COL_EMAIL, user.getEmail());
        values.put(COL_TELEPHONE, user.getTelephone());
        values.put(COL_LIEUX, user.getLieux());
        values.put(COL_CARTE, user.getCarte());
        values.put(COL_ADRESSE, user.getAdresse());
        values.put(COL_IMAGE, user.getImage());
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_USER, null, values);
    }

    public int updateUser(int id, User user){
        //La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simplement préciser quel livre on doit mettre à jour grâce à l'ID
        ContentValues values = new ContentValues();
        values.put(COL_NAME, user.getName());
        values.put(COL_EMAIL, user.getEmail());
        values.put(COL_TELEPHONE, user.getTelephone());
        values.put(COL_LIEUX, user.getLieux());
        values.put(COL_CARTE, user.getCarte());
        values.put(COL_ADRESSE, user.getAdresse());
        values.put(COL_IMAGE, user.getImage());
        return bdd.update(TABLE_USER, values, COL_ID + " = " +id, null);
    }

    public int removeUserWithID(int id){
        //Suppression d'un livre de la BDD grâce à l'ID
        return bdd.delete(TABLE_USER, COL_ID + " = " +id, null);
    }

    public User getUserWithEmail(String email){
        //Récupère dans un Cursor les valeurs correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        Cursor c = bdd.query(TABLE_USER, new String[] {COL_ID, COL_NAME, COL_EMAIL,COL_TELEPHONE,COL_LIEUX,COL_CARTE,COL_ADRESSE,COL_IMAGE}, COL_EMAIL + " LIKE \"" + email +"\"", null, null, null, null);
        return cursorToUser(c);
    }

    //Cette méthode permet de convertir un cursor en un livre
    private User cursorToUser(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé un livre
        User user = new User();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        user.setId(c.getString(NUM_COL_ID));
        user.setName(c.getString(NUM_COL_NAME));
        user.setEmail(c.getString(NUM_COL_EMAIL));
        user.setTelephone(c.getString(NUM_COL_TELEPHONE));
        user.setLieux(c.getString(NUM_COL_LIEUX));
        user.setCarte(c.getString(NUM_COL_CARTE));
        user.setAdresse(c.getString(NUM_COL_ADRESSE));
        user.setImage(c.getString(NUM_COL_IMAGE));
        //On ferme le cursor
        c.close();

        //On retourne le livre
        return user;
    }
}
